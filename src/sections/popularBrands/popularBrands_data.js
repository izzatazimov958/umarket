import nokia from "../../images/popularBrands/nokia.svg";
import apple from "../../images/popularBrands/apple.svg";
import samsung from "../../images/popularBrands/samsung.svg";
import huawei from "../../images/popularBrands/huawei.svg";
import lg from "../../images/popularBrands/lg.svg";
import xiaomi from "../../images/popularBrands/xiaomi.svg";

export const data = [
  {
    id: "c1",
    ctg_name: {
      text: "Телефоны",
    },
    data: [
      {
        product_id: "p1",
        img: nokia,
      },
      {
        product_id: "p2",
        img: apple,
      },
      {
        product_id: "p3",
        img: samsung,
      },
      {
        product_id: "p4",
        img: samsung,
      },
      {
        product_id: "p5",
        img: samsung,
      },
      {
        product_id: "p6",
        img: samsung,
      },
      {
        product_id: "p7",
        img: huawei,
      },
      {
        product_id: "p8",
        img: lg,
      },
      {
        product_id: "p9",
        img: xiaomi,
      },
      {
        product_id: "p10",
        img: xiaomi,
      },
      {
        product_id: "p11",
        img: xiaomi,
      },
      {
        product_id: "p12",
        img: xiaomi,
      },
    ],
  },
  {
    id: "c2",
    ctg_name: {
      text: "Аксессуары",
    },
    data: [
      {
        product_id: "p1",
        img: apple,
      },
      {
        product_id: "p2",
        img: nokia,
      },
      {
        product_id: "p3",
        img: samsung,
      },
      {
        product_id: "p4",
        img: samsung,
      },
      {
        product_id: "p5",
        img: samsung,
      },
      {
        product_id: "p6",
        img: samsung,
      },
      {
        product_id: "p7",
        img: huawei,
      },
      {
        product_id: "p8",
        img: lg,
      },
      {
        product_id: "p9",
        img: xiaomi,
      },
      {
        product_id: "p10",
        img: xiaomi,
      },
      {
        product_id: "p11",
        img: xiaomi,
      },
      {
        product_id: "p12",
        img: xiaomi,
      },
    ],
  },
  {
    id: "c3",
    ctg_name: {
      text: "Premium",
    },
    data: [
      {
        product_id: "p1",
        img: nokia,
      },
      {
        product_id: "p2",
        img: samsung,
      },
      {
        product_id: "p3",
        img: apple,
      },
      {
        product_id: "p4",
        img: samsung,
      },
      {
        product_id: "p5",
        img: samsung,
      },
      {
        product_id: "p6",
        img: samsung,
      },
      {
        product_id: "p7",
        img: huawei,
      },
      {
        product_id: "p8",
        img: lg,
      },
      {
        product_id: "p9",
        img: xiaomi,
      },
      {
        product_id: "p10",
        img: xiaomi,
      },
      {
        product_id: "p11",
        img: xiaomi,
      },
      {
        product_id: "p12",
        img: xiaomi,
      },
    ],
  },
  {
    id: "c4",
    ctg_name: {
      text: "Спорт",
    },
    data: [
      {
        product_id: "p1",
        img: nokia,
      },
      {
        product_id: "p2",
        img: apple,
      },
      {
        product_id: "p3",
        img: samsung,
      },
      {
        product_id: "p4",
        img: samsung,
      },
      {
        product_id: "p5",
        img: samsung,
      },
      {
        product_id: "p6",
        img: samsung,
      },
      {
        product_id: "p7",
        img: lg,
      },
      {
        product_id: "p8",
        img: huawei,
      },
      {
        product_id: "p9",
        img: xiaomi,
      },
      {
        product_id: "p10",
        img: xiaomi,
      },
      {
        product_id: "p11",
        img: xiaomi,
      },
      {
        product_id: "p12",
        img: xiaomi,
      },
    ],
  },
  {
    id: "c5",
    ctg_name: {
      text: "Игрушки",
    },
    data: [
      {
        product_id: "p1",
        img: nokia,
      },
      {
        product_id: "p2",
        img: apple,
      },
      {
        product_id: "p3",
        img: samsung,
      },
      {
        product_id: "p4",
        img: samsung,
      },
      {
        product_id: "p5",
        img: samsung,
      },
      {
        product_id: "p6",
        img: samsung,
      },
      {
        product_id: "p7",
        img: huawei,
      },
      {
        product_id: "p8",
        img: lg,
      },
      {
        product_id: "p9",
        img: xiaomi,
      },
      {
        product_id: "p10",
        img: xiaomi,
      },
      {
        product_id: "p11",
        img: xiaomi,
      },
      {
        product_id: "p12",
        img: xiaomi,
      },
    ],
  },
  {
    id: "c6",
    ctg_name: {
      text: "Красота",
    },
    data: [
      {
        product_id: "p1",
        img: nokia,
      },
      {
        product_id: "p2",
        img: apple,
      },
      {
        product_id: "p3",
        img: samsung,
      },
      {
        product_id: "p4",
        img: samsung,
      },
      {
        product_id: "p5",
        img: samsung,
      },
      {
        product_id: "p6",
        img: samsung,
      },
      {
        product_id: "p7",
        img: huawei,
      },
      {
        product_id: "p8",
        img: lg,
      },
      {
        product_id: "p9",
        img: xiaomi,
      },
      {
        product_id: "p10",
        img: xiaomi,
      },
      {
        product_id: "p11",
        img: xiaomi,
      },
      {
        product_id: "p12",
        img: apple,
      },
    ],
  },
  {
    id: "c7",
    ctg_name: {
      text: "Книги",
    },
    data: [
      {
        product_id: "p1",
        img: nokia,
      },
      {
        product_id: "p2",
        img: apple,
      },
      {
        product_id: "p3",
        img: samsung,
      },
      {
        product_id: "p4",
        img: samsung,
      },
      {
        product_id: "p5",
        img: xiaomi,
      },
      {
        product_id: "p6",
        img: samsung,
      },
      {
        product_id: "p7",
        img: huawei,
      },
      {
        product_id: "p8",
        img: lg,
      },
      {
        product_id: "p9",
        img: xiaomi,
      },
      {
        product_id: "p10",
        img: xiaomi,
      },
      {
        product_id: "p11",
        img: xiaomi,
      },
      {
        product_id: "p12",
        img: xiaomi,
      },
    ],
  },
  {
    id: "c8",
    ctg_name: {
      text: "Обувь",
    },
    data: [
      {
        product_id: "p1",
        img: nokia,
      },
      {
        product_id: "p2",
        img: apple,
      },
      {
        product_id: "p3",
        img: samsung,
      },
      {
        product_id: "p4",
        img: samsung,
      },
      {
        product_id: "p5",
        img: xiaomi,
      },
      {
        product_id: "p6",
        img: apple,
      },
      {
        product_id: "p7",
        img: huawei,
      },
      {
        product_id: "p8",
        img: lg,
      },
      {
        product_id: "p9",
        img: xiaomi,
      },
      {
        product_id: "p10",
        img: xiaomi,
      },
      {
        product_id: "p11",
        img: xiaomi,
      },
      {
        product_id: "p12",
        img: xiaomi,
      },
    ],
  },
];
