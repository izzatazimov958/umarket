import React, { useState } from "react";
import STitle from "../../components/sectionTitle/STitle";
import { data } from "./popularBrands_data";
import "./popularbrands.scss";

const PopularBrands = () => {
  const [activeBtn, setActiveBtn] = useState(0);

  return (
    <div className="container">
      <div className="popularBrands">
        <STitle text="Популярные бренды" />
        <div className="btns-wrapper">
          {data.map((item, idx) => (
            <button
              key={item.id}
              onClick={() => setActiveBtn(idx)}
              className={activeBtn === idx ? "active" : null}
            >
              {item.ctg_name.text}
            </button>
          ))}
        </div>

        <div className="cards-wrapper">
          {data[activeBtn].data.map((item) => (
            <div className="card" key={item.product_id}>
              <img src={item.img} alt={item.img} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PopularBrands;
