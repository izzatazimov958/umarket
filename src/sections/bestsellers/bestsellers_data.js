import phone from "../../images/bestsellers/product1.svg";
import phone2 from "../../images/bestsellers/product2.svg";
import star1 from "../../images/bestsellers/Star.svg";
import heart from "../../images/headerImages/middleNavImages/heart-icon.svg";
import sync from "../../images/headerImages/middleNavImages/sync_alt-icon.svg";

export const data = [
  {
    id: 1,
    img: phone,
    title: `Samsung Galaxy A41 Red 64 GB`,
    price: "3 144 000 сум",
    installment: "От 385 000 сум/12 мес",
    stars: [star1, star1, star1, star1, star1],
    heart,
    sync,
  },
  {
    id: 2,
    img: phone,
    title: `Samsung Galaxy A41 Red 64 GB`,
    price: "3 144 000 сум",
    installment: "От 385 000 сум/12 мес",
    stars: [star1, star1, star1, star1, star1],
    heart,
    sync,
  },
  {
    id: 3,
    img: phone,
    title: `Samsung Galaxy A41 Red 64 GB`,
    price: "3 144 000 сум",
    installment: "От 385 000 сум/12 мес",
    stars: [star1, star1, star1, star1, star1],
    heart,
    sync,
  },
  {
    id: 4,
    img: phone2,
    title: `Samsung Galaxy A41 Red 64 GB`,
    price: "3 144 000 сум",
    installment: "От 385 000 сум/12 мес",
    stars: [star1, star1, star1, star1, star1],
    heart,
    sync,
  },
];
