import AddToCartBtn from "../../components/addToCart/AddToCartBtn";
import STitle from "../../components/sectionTitle/STitle";
import './buyerschoice.scss';
import { data } from "./buyersChoice_data.js";

const BuyersChoice = () => {
  return (
    <div className="container">
      <div className="buyersChoice">
        <STitle text="Выбор покупателей" />
        <div className="card-wrapper">
          {data.map((item) => (
            <div className="card" key={item.id}>
              <div className="hover-div">Быстрый просмотр</div>
              <img className="cardImage" src={item.img} alt="product" />
              <div className="card-info">
                <p className="card-title">{item.title}</p>
                <div className="card-price">
                  <span>{item.price}</span>
                  <span>{item.installment}</span>
                </div>
                <div className="card-stars">
                  {item.stars.map((star, idx) => (
                    <img src={star} alt="star" key={idx} />
                  ))}
                </div>
                <div className="btns-wrapper">
                  <AddToCartBtn text="В корзину" />
                  <div className="card-icons">
                    <img src={item.sync} alt="sync" />
                    <img src={item.heart} alt="heart" />
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default BuyersChoice;
