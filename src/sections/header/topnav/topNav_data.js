import phone from "../../../images/headerImages/topNavImages/phone-icon.svg";
import location from "../../../images/headerImages/topNavImages/location_on-icon.svg";
import rusFlag from "../../../images/headerImages/topNavImages/rus-flag.svg";

export const menu = [
  {
    name: "Магазины",
    link: "outlets",
  },
  {
    name: "Оставить отзыв",
    link: "feedback",
  },
  {
    name: "Доставка",
    link: "delivery",
  },
];

export const info = [
  {
    id: 1,
    text: "+998 97 778-17-08",
    img: phone,
  },
  {
    id: 2,
    text: "Ташкент",
    img: location,
  },
  {
    id: 3,
    text: [
      {
        id: 1,
        lang: "Рус",
        flag: rusFlag,
      },
      {
        id: 2,
        lang: "Uzb",
        flag: `https://cdn.countryflags.com/thumbs/uzbekistan/flag-round-250.png`,
      },
      {
        id: 3,
        lang: "Eng",
        flag: `https://i.pinimg.com/736x/39/5e/24/395e24a00bcb9fcbe9e037066c75991c.jpg`,
        
        // flag: `https://pngset.com/images/us-flag-vector-us-flag-circle-symbol-american-flag-rug-logo-transparent-png-472376.png`
      },
    ],
  },
];
