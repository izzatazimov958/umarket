import { useState } from "react";
import { NavLink } from "react-router-dom";
import "./topnav.scss";
import { menu, info } from "./topNav_data";

const TopNav = () => {
  const [img, setImg] = useState(0);
  return (
    <div className="topNav">
      <div className="container">
        <ul className="topNav__menu">
          {menu.map((item, index) => {
            return (
              <li key={index}>
                <NavLink to={item.link}>{item.name}</NavLink>
              </li>
            );
          })}
        </ul>
        <ul className="topNav__info">
          {info.map((item, index) => (
            <li key={item.id && item.id}>
              {item.img && (
                <img src={item.img && item.img} alt={item?.text && item.text} />
              )}
              {item.id === 3 ? (
                <>
                  <img src={item?.text[img].flag} alt={item.lang} />
                  <select onChange={(e) => setImg(e.target.selectedIndex)}>
                    {item.text.map((lang) => (
                      <option
                        className={lang.lang ? `${lang.lang} option` : "option"}
                        value={lang?.lang}
                        key={lang.id}
                      >
                        {lang?.lang}
                      </option>
                    ))}
                  </select>
                </>
              ) : (
                <span>{item?.text && item.text}</span>
              )}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default TopNav;
