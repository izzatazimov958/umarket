import { logo, search, img, options_array } from "./middleNav_data";
import "./middlenav.scss";
import { useState } from "react";
import SignUp from "../../../components/registration/signUp/SignUp";
import SignIn from "../../../components/registration/signIn/SignIn";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const MiddleNav = () => {
  const navigate = useNavigate();
  const optArr = options_array;
  const [modal, setModal] = useState(false);
  const isRegistered = useSelector((state) => state.isRegistered.isRegistered);

  return (
    <div className="container">
      <div className="middleNav">
        <div className="logo" onClick={() => navigate("/")}>
          <img src={logo} alt="uMarket" />
        </div>
        <div className="search_input">
          <img src={search} alt="search" />
          <input type="text" placeholder="Поиск по товарам" />
          <img src={img} alt="imgSearch" />
        </div>
        <ul className="options">
          <li className="option">
            <img src={optArr[0].icon} alt={optArr[0].name} />
            <span>{optArr[0].name}</span>
          </li>
          <li className="option">
            <img src={optArr[1].icon} alt={optArr[1].name} />
            <span>{optArr[1].name}</span>
          </li>
          <li className="option">
            <img src={optArr[2].icon} alt={optArr[2].name} />
            <span>{optArr[2].name}</span>
          </li>
          <li className="option" onClick={() => setModal(!modal)}>
            <img src={optArr[3].icon} alt={optArr[3].name} />
            <span>{optArr[3].name}</span>
            {modal && !isRegistered && <SignUp open={modal} />}
            {modal && isRegistered && <SignIn open={modal} />}
          </li>
        </ul>
      </div>
    </div>
  );
};

export default MiddleNav;
