import logoIcon from './../../../images/headerImages/middleNavImages/site-logo.svg'  
import searchIcon from './../../../images/headerImages/middleNavImages/search-icon.svg'  
import imgIcon from './../../../images/headerImages/middleNavImages/search-img-icon.svg'  
import cartIcon from './../../../images/headerImages/middleNavImages/shopping_cart-icon.svg'  
import heartIcon from './../../../images/headerImages/middleNavImages/heart-icon.svg'  
import syncIcon from './../../../images/headerImages/middleNavImages/sync_alt-icon.svg'  
import loginIcon from './../../../images/headerImages/middleNavImages/login-icon.svg'  


export const logo = logoIcon
export const search = searchIcon
export const img = imgIcon

export const options_array = [
  {
    id: 1,
    name: 'Корзина',
    icon: cartIcon
  },
  {
    id: 2,
    name: 'Избранные',
    icon: heartIcon
  },
  {
    id: 3,
    name: 'Сравнение',
    icon: syncIcon
  },
  {
    id: 4,
    name: 'Войти',
    icon: loginIcon
  }
]

