import React from 'react'
import BottomNav from './bottomNav/BottomNav'
import MiddleNav from './middleNav/MiddleNav'
import TopNav from './topnav/TopNav'

const Header = ()=> {
  return (
    <div className='header'>
      <TopNav />
      <MiddleNav />
      <BottomNav />
    </div>
  )
}

export default Header