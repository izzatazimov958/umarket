import React from "react";
import './bottomnav.scss'

const menu_array = [
  {
    text: "Акции и скидки",
    link: "/",
  },
  {
    text: "Смартфоны и гаджеты",
    link: "/",
  },
  {
    text: "Телевизоры и аудио",
    link: "/",
  },
  {
    text: "Техника для кухни",
    link: "/",
  },
  {
    text: "Красота и здоровье",
    link: "/",
  },
  {
    text: "Ноутбуки и компьютеры",
    link: "/",
  },
];

const BottomNav = () => {
  return (
    <div className="bottomNav">
      <div className="container">
        <ul>
          {menu_array.map((item, idx) => (
            <li key={idx}>
              <a href={item.link}>{item.text}</a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default BottomNav;
