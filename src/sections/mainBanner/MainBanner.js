
import './mainbanner.scss'
import mainBanner from '../../images/mainBannerImages/banner.svg'

const MainBanner = () => {
  return (
    <div className="container">
      <div className="mainBanner">
        <img src={mainBanner} alt="mainBanner" />
      </div>
    </div>
  );
};

export default MainBanner;
