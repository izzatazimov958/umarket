import React from "react";
import { data } from "./footerBottom_data";
import "./footerbottom.scss";

const FooterBottom = () => {
  return (
    <div className="container">
      <div className="footerBottom">
        <div className="left">
          <div className="topTexts">
            {data[0].map((item) => (
              <div className="item" key={item.id}>
                <span className="title">{item.name.text}</span>
                {item.data.map((item, idx) => (
                  <span className="subtitle" key={idx}>
                    {item}
                  </span>
                ))}
              </div>
            ))}
          </div>

          <div className="bottomTexts">
            {data[1].map((item) => (
              <div className="item" key={item.id}>
                <span className="title">{item.name.text}</span>
                {item.data.map((item, idx) => (
                  <span className="subtitle" key={idx}>
                    {item}
                  </span>
                ))}
              </div>
            ))}
          </div>
        </div>

        <div className="right">
          <div className="payments-wrapper">
            <p className="title">{data[2][0].payment_methods}</p>

            <div className="img-div">
              {data[2][0].imgs.map((item, idx) => (
                <img src={item} alt="payments" key={idx} />
              ))}
            </div>
          </div>
          {/*  */}
          <div className="socials-wrapper">
            <p className="title">{data[3][0].socials}</p>

            <div className="img-div">
              {data[3][0].icons.map((item, idx) => (
                <img src={item} alt="icons" key={idx} />
              ))}
            </div>
          </div>
        </div>

        <p className="bottom-text">
          <span>UdevsMarket.uz</span>
          Все права защищены
        </p>
      </div>
    </div>
  );
};

export default FooterBottom;
