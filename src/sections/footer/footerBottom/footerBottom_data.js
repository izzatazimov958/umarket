import payme from "../../../images/footer/footerBottomImages/payMe.svg";
import humo from "../../../images/footer/footerBottomImages/humo.svg";
import uzCard from "../../../images/footer/footerBottomImages/uzCard.svg";

import linkedin from "../../../images/footer/footerBottomImages/linkedin.svg";
import instagram from "../../../images/footer/footerBottomImages/instagram.svg";
import twitter from "../../../images/footer/footerBottomImages/twitter.svg";
import facebook from "../../../images/footer/footerBottomImages/facebook.svg";

export const data = [
  [
    {
      id: "1",
      name: {
        text: "Компания",
      },
      data: ["О компании", "Адреса магазинов"],
    },
    {
      id: "2",
      name: {
        text: "Информация",
      },
      data: ["Рассрочка", "Доставка", "Бонусы"],
    },
    {
      id: "3",
      name: {
        text: "Помощь покупателю",
      },
      data: [
        "Вопросы и ответы",
        "Как сделать заказ на сайте",
        "Обмен и возврат",
      ],
    },
  ],
  [
    {
      id: "1",
      name: {
        text: "Единый кол центр",
      },
      data: ["+9980 71-54-60-60"],
    },
    {
      id: "2",
      name: {
        text: "Почта для пожеланий и предложений",
      },
      data: ["info@udevsmarket.com"],
    },
  ],
  [
    {
      payment_methods: "Способ  оплаты",
      imgs: [payme, humo, uzCard],
    },
  ],
  [
    {
      socials: "Мы в социальных сетях",
      icons: [linkedin, instagram, twitter, facebook],
    },
  ],
];
