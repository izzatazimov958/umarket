import { footerTopData } from "./footerTop_data.js";
import "./footertop.scss";

const footerTopTexts = [
  {
    title: ` mobile application`,
    subtitle: `Заказывайте через  
    мобильное приложение`,
    desc: `Отсканируйте QR-код 
    и установите приложение`,
  },
];

const FooterTop = () => {
  return (
    <div className="footerTop">
      <div className="container">
        <div className="left">
          <div className="footer-logo">
            <img src={footerTopData.logo} alt="uMarket" />
            <span>{footerTopTexts[0].title}</span>
          </div>
          <p className="subtitle">{footerTopTexts[0].subtitle}</p>
          <div className="downloads">
            <img src={footerTopData.googlePlay} alt="googlePlay" />
            <img src={footerTopData.appStore} alt="appStore" />
          </div>
        </div>
        <div className="right">
          <div className="qr-code">
            <img src={footerTopData.qrCode} alt="qrCode" />
          </div>
          <span>{footerTopTexts[0].desc}</span>
        </div>
      </div>
    </div>
  );
};

export default FooterTop;
