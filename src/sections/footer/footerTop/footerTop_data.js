import logo from "../../../images/headerImages/middleNavImages/site-logo.svg";
import appStore from "../../../images/footer/footerTopImages/appStore.svg";
import googlePlay from "../../../images/footer/footerTopImages/googlePlay.svg";
import qrCode from "../../../images/footer/footerTopImages/qrCode.svg";

export const footerTopData = {
  logo,
  appStore,
  googlePlay,
  qrCode,
};
