import product1 from "../../images/homeAppliances/product1.svg";
import product2 from "../../images/homeAppliances/product2.svg";
import product3 from "../../images/homeAppliances/product3.svg";
import product4 from "../../images/homeAppliances/product4.svg";
import product5 from "../../images/homeAppliances/product5.svg";

export const data = [
  {
    text: `Встраиваемая 
техника`,
    img: product1,
    link: "/",
  },
  {
    text: "Пылесосы",
    img: product2,
    link: "/",
  },
  {
    text: `Стиральные 
    машины`,
    img: product3,
    link: "/",
  },
  {
    text: "Холодильники",
    img: product4,
    link: "/",
  },
  {
    text: "Кондиционеры",
    img: product5,
    link: "/",
  },
];
