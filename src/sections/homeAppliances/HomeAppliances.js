import GreyCard from "../../components/greyCard/GreyCard";
import STitle from "../../components/sectionTitle/STitle";
import "./homeappliances.scss";
import { data } from "./homeAppliances_data.js";

const HomeAppliances = () => {
  return (
    <div className="container">
      <section className="homeAppliances">
        <STitle text="Техника для дома" />
        <div className="card-wrapper">
          {data.map((item, index) => {
            return <GreyCard img={item.img} title={item.text} key={index} />;
          })}
        </div>
      </section>
    </div>
  );
};

export default HomeAppliances;
