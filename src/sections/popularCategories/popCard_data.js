import phoneImg from "../../images/popularCategories/phone.svg";
import screenImg from "../../images/popularCategories/screen.svg";
import laptopImg from "../../images/popularCategories/laptop.svg";
import watchImg from "../../images/popularCategories/watch.svg";

export const data = [
  {
    id: 1,
    link: "/",
    text: "Смартфоны",
    img: phoneImg,
  },
  {
    id: 2,
    link: "/",
    text: "Мониторы",
    img: screenImg,
  },
  {
    id: 3,
    link: "/",
    text: "Компьютеры",
    img: laptopImg,
  },
  {
    id: 4,
    link: "/",
    text: "Аксессуары",
    img: watchImg,
  },
  
];
