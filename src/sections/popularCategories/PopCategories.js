import GreyCard from "../../components/greyCard/GreyCard";
import STitle from "../../components/sectionTitle/STitle";
import { data } from "./popCard_data";
import "./popcategories.scss";

const PopCategories = () => {
  return (
    <div className="container">
      <div className="popCategories">
        <STitle text="Популярные категории" />
        <div className="card-wrapper">
          {data.map((item) => (
            <GreyCard
              width="300px"
              height="232px"
              img={item.img}
              title={item.text}
              padding="16px 13px"
              key={item.id}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default PopCategories;
