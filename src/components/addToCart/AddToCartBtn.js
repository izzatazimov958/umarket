import "./addtocartbtn.scss";
import addToCart from "../../images/headerImages/middleNavImages/shopping_cart-icon.svg";

const AddToCartBtn = (props) => {
  return (
    <button className="addToCartBtn">
      <img src={addToCart} alt="addToCart" />{props.text}
    </button>
  );
};

export default AddToCartBtn;
