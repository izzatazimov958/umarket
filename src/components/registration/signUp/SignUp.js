import "./signup.scss";
import BasicModal from "../modal";
import google from "../../../images/register/google.svg";
// import facebook from "../../../images/register/facebook.svg";
import facebook from "../../../Assets/icons/facebookLogin.svg"; //eslint-disable-line
import FacebookLogin from "../../../Assets/icons/realFacebook.png";

import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { isRegisteredAction } from "../../../redux/actions/isRegisteredAction";

const SignUp = (props) => {
  const { open } = props;
  const dispatch = useDispatch();
  const [isConfirming, setIsConfirming] = useState(false);

  return (
    <BasicModal open={open}>
      {/* <div className="registration" onClick="$emit('action')"> */}

      <form className="modal-card sign-up">
        {isConfirming === false && (
          <div className="form-content">
            <h3 className="title">Регистрация</h3>
            <input
              className="input"
              type="text"
              placeholder="Имя"
              name="name"
            />

            <input
              className="input"
              type="text"
              placeholder="Фамилия"
              name="fName"
            />
            <input
              className="input"
              type="number"
              placeholder="Номер телефона"
              name="phoneNumber"
            />
            <input
              className="input"
              type="number"
              placeholder="Введите пароль"
              name="password"
            />
            <input
              className="input"
              type="number"
              placeholder="Подтверждение пароля"
              name="confirmPassword"
            />
            <button onClick={() => setIsConfirming(true)} className="btn">
              Зарегистрироваться
            </button>
            <div className="privacy">
              <input type="checkbox" name="privacy" />
              <p>
                Согласен с <a href="/">условиями правил пользования</a>
                торговой площадкой и <a href="/">правилами возврата</a>
              </p>
            </div>
          </div>
        )}

        {isConfirming === true && (
          <div className="confirmPage form-content">
            <h3 className="title">Подтвердить пароль</h3>
            <div className="form-content">
              <input
                className="input"
                type="number"
                placeholder="Номер телефона"
                name="phoneNumber"
              />
              <input
                className="input"
                type="number"
                placeholder="Код"
                name="password"
              />

              <button onClick={() => setIsConfirming(false)} className="btn">
                Отправить
              </button>

              <span className="confirmLimit">00 : 47</span>
            </div>
          </div>
        )}

        {isConfirming === false && (
          <div className="modal-card-footer">
            <p>Или зарегистрироваться с</p>
            <div className="wrap">
              <a href="/">
                <img src={google} alt="google" />
              </a>
              <a href="/">
                <img src={FacebookLogin} alt="facebook" />
              </a>
            </div>
            <p>
              У вас есть аккаунт?{" "}
              <button onClick={() => dispatch(isRegisteredAction(true))}>
                Войти
              </button>
            </p>
          </div>
        )}
      </form>
      {/* </div> */}
    </BasicModal>

    // <div className="registration" onClick="$emit('action')">

    //   <form className="modal-card" onClickstop>
    //     <h3
    //       className="title"
    //       style={{
    //         display: isConfirming === false ? "block" : "none",
    //       }}
    //     >
    //       Регистрация
    //     </h3>
    //     <div
    //       className="form-content"
    //       style={{
    //         display: isConfirming === false ? "flex" : "none",
    //       }}
    //     >
    //       <input className="input" type="text" placeholder="Имя" name="name" />

    //       <input
    //         className="input"
    //         type="text"
    //         placeholder="Фамилия"
    //         name="fName"
    //       />
    //       <input
    //         className="input"
    //         type="number"
    //         placeholder="Номер телефона"
    //         name="phoneNumber"
    //       />
    //       <input
    //         className="input"
    //         type="number"
    //         placeholder="Введите пароль"
    //         name="password"
    //       />
    //       <input
    //         className="input"
    //         type="number"
    //         placeholder="Подтверждение пароля"
    //         name="confirmPassword"
    //       />
    //       <button className="btn" onClick="$emit('action')">
    //         Зарегистрироваться
    //       </button>
    //       <div className="privacy">
    //         <input type="checkbox" name="privacy" />
    //         <p>
    //           Согласен с <a href="/">условиями правил пользования</a>
    //           торговой площадкой и <a href="/">правилами возврата</a>
    //         </p>
    //       </div>
    //     </div>

    //     <div
    //       className="confirmPage form-content"
    //       style={{
    //         display: isConfirming === true ? "block" : "none",
    //       }}
    //     >
    //       <h3 className="title">Подтвердить пароль</h3>
    //       <div className="form-content">
    //         <input
    //           className="input"
    //           type="number"
    //           placeholder="Номер телефона"
    //           name="phoneNumber"
    //         />
    //         <input
    //           className="input"
    //           type="number"
    //           placeholder="Код"
    //           name="password"
    //         />

    //         <button className="btn" onClick="$emit('action')">
    //           Отправить
    //         </button>

    //         <span className="confirmLimit">00 : 47</span>
    //       </div>
    //     </div>

    //     <div
    //       className="modal-card-footer"
    //       style={{
    //         display: isConfirming === false ? "flex" : "none",
    //       }}
    //     >
    //       <p>Или зарегистрироваться с</p>
    //       <div className="wrap">
    //         <a href="/">
    //           <img src={google} alt="google" />
    //         </a>
    //         <a href="/">
    //           <img src={facebook} alt="facebook" />
    //         </a>
    //       </div>
    //       <p>
    //         У вас есть аккаунт? <a href="/">Войти</a>
    //       </p>
    //     </div>
    //   </form>
    // </div>
  );
};

export default SignUp;
