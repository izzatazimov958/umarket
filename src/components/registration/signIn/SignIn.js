import "./signIn.scss";
import BasicModal from "../modal";
import google from "../../../images/register/google.svg";
import facebook from "../../../images/register/facebook.svg"; //eslint-disable-line
import FacebookLogin from "../../../Assets/icons/realFacebook.png";

import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { isRegisteredAction } from "../../../redux/actions/isRegisteredAction";

const SignIn = (props) => {
  const { open } = props;
  const dispatch = useDispatch();
  const [isConfirming, setIsConfirming] = useState(false);

  const [phone, setPhone] = useState("+998");

  return (
    <BasicModal open={open}>
      {/* <div className="registration" onClick="$emit('action')"> */}

      <form className="modal-card sign-in">
        {isConfirming === false && (
          <div className="form-content">
            <h3 className="title">Добро пожаловать</h3>
            <p className="subtitle">
              Войдите с вашим номером телефона или паролем
            </p>

            <input
              className="input"
              type="text"
              name="password"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />

            <button
              onClick={() => setIsConfirming(true)}
              className={phone.length > 12 ? "btn blue" : "btn"}
            >
              {isConfirming ? "Отправить" : "Получить код"}
            </button>
          </div>
        )}

        {isConfirming === true && (
          <div className="confirmPage form-content">
            <h3 className="title">Подтвердить пароль</h3>
            <div className="form-content">
              <input
                className="input"
                type="text"
                onChange={(e) => setPhone(e.target.value)}
                name="phoneNumber"
                value={phone}
              />
              <input
                className="input"
                type="number"
                placeholder="Код"
                name="password"
              />

              <button
                onClick={() => setIsConfirming(false)}
                className={phone.length > 12 ? "btn blue" : "btn"}
              >
                {isConfirming ? "Отправить" : "Получить код"}
              </button>

              <span className="confirmLimit">00 : 47</span>
            </div>
          </div>
        )}

        {isConfirming === false && (
          <div className="modal-card-footer">
            <p>Или зарегистрироваться с</p>
            <div className="wrap">
              <a href="/">
                <img src={google} alt="google" />
              </a>
              <a href="/">
                <img src={FacebookLogin} alt="facebook" />
              </a>
            </div>
            <p>
              У вас нет аккаунта?{" "}
              <button onClick={() => dispatch(isRegisteredAction(false))}>
                Зарегистрируйтесь
              </button>
            </p>
          </div>
        )}
      </form>
      {/* </div> */}
    </BasicModal>
  );
};

export default SignIn;
