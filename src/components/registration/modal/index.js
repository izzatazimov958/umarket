import * as React from "react";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import { Button } from "@mui/material";
// import { ClosedIcon } from "@mui/icons-material";
// import Close from '@mui/icons-material/Close';
import CloseIcon from '@mui/icons-material/Close';

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 500,
  bgcolor: "background.paper",
  boxShadow: 24,
  border: "none",
  borderRadius: "8px",
  outline: "none",
};

const style2 = {
  position: 'absolute',
  top: '10px',
  right: '0',

  // backgroundColor: 'transparent',

}

function BasicModal({
  open,
  setOpen = () => {},
  handleClose,
  title,
  subTitle,
  width,
  children,
}) {
  // console.log(handleClose);

  return (
    <div>
      <Modal
        open={open}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box style={{ width: `${width}px` }} sx={style}>
          <Button  style={style2} onClick={() => setOpen(false)}>
            <CloseIcon  />
          </Button>
          <div onClick={(e) => e.stopPropagation()} className="pt-10">
            {children}
          </div>
        </Box>
      </Modal>
    </div>
  );
}

export default BasicModal;
