import { useState } from "react";

const FormInput = ({
  value,
  type,
  name,
  label,
  className,
  required = false,
  handleChange,
  placeholder,
  errorMessage,
  ...props
}) => {
  const [touched, setTouched] = useState(false);

  return (
    <div>
      {label && <label htmlFor={name}>{label}</label>}
      <input
        className={`form-input ${className}`}
        type={type}
        name={name}
        id={name}
        placeholder={placeholder}
        required={required}
        value={value}
        onChange={handleChange}
        onBlur={() => (required ? setTouched(true) : setTouched(false))} // ! required true kelsa va required parameterini qoniqtirmasa input borderi qizil ranga o'zgaradi
        onFocus={() => name === "confirmPassword" && setTouched(true)}
        touched={touched.toString()}
        {...props}
      />
      {required && errorMessage && (
        <div className="errorMessage">{errorMessage}</div>
      )}
    </div>
  );
};

export default FormInput;
