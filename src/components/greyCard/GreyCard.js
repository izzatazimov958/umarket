import "./greycard.scss";

const GreyCard = (props) => {
  return (
    <div
      className="greyCard"
      style={
        props.width && {
          minWidth: props.width,
          maxWidth: props.width,
          height: props.height,
          padding: props.padding,
        }
      }
    >
      <img src={props.img} alt={props.title} />
      <h3>{props.title}</h3>
    </div>
  );
};

export default GreyCard;
