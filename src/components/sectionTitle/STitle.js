
import './stitle.scss'

const STitle=(props)=> {
  return (
    <h3 className="STitle">{props.text}</h3>
  )
}

export default STitle