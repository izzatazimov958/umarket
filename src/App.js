import { Route, Routes } from "react-router-dom";
import "./App.scss";
import Delivery from "./pages/Delivery/Delivery";
import Feedback from "./pages/Feedback/Feedbacks";
import Main from "./pages/Main";
import Outlets from "./pages/Outlets/Outlets";

import FooterBottom from "./sections/footer/footerBottom/FooterBottom";
import FooterTop from "./sections/footer/footerTop/FooterTop";
import Header from "./sections/header/Header";

const style = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  margin: '0 auto',
  width: '500px',
  height: '300px'

}

function App() {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Main />} />

        <Route path="feedback" element={<Feedback />} />
        <Route path="delivery" element={<Delivery />} />
        <Route path="outlets" element={<Outlets />} />
        <Route path="*" element={<div style={style}> <h2>sorry no page is available in this url</h2></div>} />

      </Routes>
      <FooterTop />
      <FooterBottom />
    </>
  );
}

export default App;
