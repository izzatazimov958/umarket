const initialState = {
  isRegistered: true,
};

const isRegisteredReducer = (state = initialState, action) => {
  switch (action.type) {
    case "IS_REGISTERED":
      return {
        ...state,
        isRegistered: action.payload,
      };

    default:
      return state;
  }
};

export default isRegisteredReducer;
