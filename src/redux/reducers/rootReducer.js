import { combineReducers } from "redux";
import isRegisteredReducer from "./isRegisteredReducer";

const rootReducer = combineReducers({
    // Add reducers here
    isRegistered: isRegisteredReducer
});

export default rootReducer;