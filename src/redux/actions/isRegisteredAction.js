export const isRegisteredAction = (value) => {
  return {
      type: 'IS_REGISTERED',
      payload: value
  };
}