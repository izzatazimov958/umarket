import React from "react";
import MainBanner from "../../sections/mainBanner/MainBanner";
import PopCategories from "../../sections/popularCategories/PopCategories";
import Bestsellers from "../../sections/bestsellers/Bestsellers";
import HomeAppliances from "../../sections/homeAppliances/HomeAppliances";
import BuyersChoice from "../../sections/buyersChoice/BuyersChoice";
import PopularBrands from "../../sections/popularBrands/PopularBrands";

const Main = () => {
  return (
    <>
      <MainBanner />
      <PopCategories />
      <Bestsellers />
      <HomeAppliances />
      <BuyersChoice />
      <PopularBrands />
    </>
  );
};

export default Main;
