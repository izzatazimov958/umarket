import React from "react";
import "./delivery.scss";

const Delivery = () => {
  const subtitle = `Осуществляет доставку товаров по городу Ташкент. Доставка производится в течение 48 часов с момента подтверждения заказа покупателем.`;
  const card_data = [
    {
      id: 1,
      title: "Доставка мелкой бытовой техники и электроники",
      price: "30 000 сум",
    },
    {
      id: 2,
      title: "Доставка крупной бытовой техники по городу Ташкент до подъезда",
      price: "Бесплатно",
    },
    {
      id: 3,
      title: "Доставка мелкой бытовой техники и электроники",
      price: "30 000 сум",
    },
    {
      id: 4,
      title: "Доставка мелкой бытовой техники и электроники",
      price: "30 000 сум",
    },
    {
      id: 5,
      title: "Доставка крупной бытовой техники по городу Ташкент до подъезда",
      price: "Бесплатно",
    },
    {
      id: 6,
      title: "Доставка мелкой бытовой техники и электроники",
      price: "30 000 сум",
    },
  ];

  return (
    <section className="container deliver">
      <h1 className="deliver__heading">Доставка</h1>
      <p className="deliver__text">{subtitle} </p>
      <ul className="deliver-list">
        {card_data.map((item) => (
          <li className="deliver-list__item" key={item.id}>
            <p className="deliver-type">{item.title}</p>
            <p className="deliver-price">{item.price}</p>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default Delivery;
