import React from "react";
import "./outlets.scss";
// import { Goodzone } from "../../Assets/images";
import Goodzone from "../../Assets/images/goodzone.png";
// import Map from "../../components/Map";
import Map from "../../components/Map/map";

import {
  MapIcon,
  GpsIcon,
  ClockIcon,
  PhoneIcon,
} from "../../Assets/icons/icons";

const Outlets = () => {
  const item_data = [
    {
      id: 1,
      title: "Goodzone Сергели",
      location:
        "г. Ташкент, Сергелийский район, Янги Сергели Ориентир: Напротив входа в машинный базар",
      hours: "Часы работы: с 10:00 до 21:00 (без выходных)",
    },
    {
      id: 2,
      title: "Goodzone Сергели",
      location:
        "г. Ташкент, Сергелийский район, Янги Сергели Ориентир: Напротив входа в машинный базар",
      hours: "Часы работы: с 10:00 до 21:00 (без выходных)",
    },
    {
      id: 3,
      title: "Goodzone Сергели",
      location:
        "г. Ташкент, Сергелийский район, Янги Сергели Ориентир: Напротив входа в машинный базар",
      hours: "Часы работы: с 10:00 до 21:00 (без выходных)",
    },
    {
      id: 4,
      title: "Goodzone Сергели",
      location:
        "г. Ташкент, Сергелийский район, Янги Сергели Ориентир: Напротив входа в машинный базар",
      hours: "Часы работы: с 10:00 до 21:00 (без выходных)",
    },
    {
      id: 5,
      title: "Goodzone Сергели",
      location:
        "г. Ташкент, Сергелийский район, Янги Сергели Ориентир: Напротив входа в машинный базар",
      hours: "Часы работы: с 10:00 до 21:00 (без выходных)",
    },
  ];
  return (
    <section className="container store">
      <h1 className="store__heading">Магазины</h1>
      <ul className="store-address__list">
        {item_data.map((item) => (
          <li className="store-address__list-item" key={item.id}>
            <p className="store-address__name">{item.title} </p>
            <div>
              <p className="store-address__text">{item.location}</p>
            </div>
            <p className="store-address__text">
            {item.hours}
            </p>
          </li>
        ))}
      </ul>
      <div className="store-info flex justify-between">
        <div className="store-info__more">
          <img className="store-info__img" src={Goodzone} alt="goodzone" />
          <p className="store-info__address flex justify-between">
            <GpsIcon />
            <span>г. Ташкент, Сергелийский р-н, Янги Сергели</span>
          </p>
          <p className="store-info__address flex justify-between">
            <MapIcon />
            <span>Напротив входа в машинный базар</span>
          </p>
          <p className="store-info__work-time flex justify-between">
            <ClockIcon />
            <span>с 10:00 до 21:00 (без выходных)</span>
          </p>
          <p className="store-info__tel flex justify-between">
            <PhoneIcon />
            <span>71-207-03-07</span>
          </p>
        </div>
        <Map className="store-info__map" />
      </div>
    </section>
  );
};

export default Outlets;
