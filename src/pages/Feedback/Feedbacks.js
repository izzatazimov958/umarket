import React, { useState } from "react";
import FormInput from "../../components/FormInput/input";
import "./feedbacks.scss";

const Feedback = () => {
  const inputsValues = {
    fullName: "",
    email: "",
    phoneNumber: "+998",
    store: "",
    feedbackType: "",
    comment: "",
  };

  const [values, setValues] = useState(inputsValues);

  return (
    <section className="container feedbacks">
      <h1 className="feedbacks__heading">Ваш отзыв о Goodzone</h1>
      <p className="feedbacks__text">
        Благодарим Вас за то, что Вы посетили наш интернет-магазин. Мы
        внимательно отслеживаем все этапы работы магазинов нашей торговой сети и
        будем очень признательны, если вы оставите отзыв о нашей работе.
      </p>
      <form className="feedbacks-form">
        <FormInput
          className="feedbacks-form__input"
          required
          value={values.fullName}
          type="text"
          name="fullName"
          label="Ф.И.О."
          placeholder="Напишите полное имя"
          handleChange={(e) => {
            setValues({ ...values, fullName: e.target.value });
          }}
        />
        <FormInput
          className="feedbacks-form__input"
          required
          value={values.email}
          type="email"
          name="email"
          label="Электронный адрес"
          placeholder="Напишите электронный адрес"
          handleChange={(e) => {
            setValues({ ...values, email: e.target.value });
          }}
        />
        <FormInput
          className="feedbacks-form__input"
          required
          value={values.phoneNumber}
          type="text"
          name="phoneNumber"
          label="Номер телефона"
          placeholder=""
          handleChange={(e) => {
            setValues({ ...values, phoneNumber: e.target.value });
          }}
        />
        <FormInput
          className="feedbacks-form__input"
          required
          value={values.store}
          type="text"
          name="store"
          label="Магазин"
          placeholder="Выберите  магазин"
          handleChange={(e) => {
            setValues({ ...values, store: e.target.value });
          }}
        />
        <FormInput
          className="feedbacks-form__input"
          required
          value={values.feedbackType}
          type="text"
          name="feedbackType"
          label="Тип отзыва"
          placeholder="Выберите  тип"
          handleChange={(e) => {
            setValues({ ...values, feedbackType: e.target.value });
          }}
        />
        <div>
          <label htmlFor="comment">Комментарии</label>
          <textarea
            className="feedbacks-form__textarea"
            name="comment"
            id="comment"
            cols="30"
            rows="10"
            onChange={(e) => setValues({ ...values, comment: e.target.value })}
            value={values.comment}
          />
        </div>
        <button
          // className="feedbacks-form__btn "
          className={
            values.fullName &&
            values.email &&
            values.phoneNumber &&
            values.store &&
            values.feedbackType &&
            values.comment
              ? "feedbacks-form__btn btn blue"
              : "feedbacks-form__btn btn"
          }
          type="button"
          onClick={() => {
            console.log(values);
            setValues(inputsValues);
          }}
        >
          Отправить
        </button>
      </form>
    </section>
  );
};

export default Feedback;
